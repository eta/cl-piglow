(defpackage cl-piglow
  (:nicknames :piglow)
  (:use :cl)
  (:export :*piglow-show-automatically* :*piglow* :+i2c-slave+ :+piglow-address+
           :+led-addresses+ :+led-colours+ :i2cize-stream :open-i2c-device
           :write-i2c-octet :write-i2c-octets :piglow-reset :open-piglow
           :piglow-show :piglow-set-led :piglow-set-leg :piglow-set-colour
           :demo-fade-each-led :demo-cycle-colours :*demo-max-brightness*
           :+led-colour-names+ :demo-circular-colours))
