;;;; Pretty colourful demonstration functions

(in-package :cl-piglow)

(defvar *demo-max-brightness* 100
  "Maximum brightness to be used in demos (to avoid burning your eyes out).")

(defun demo-fade-each-led ()
  "Fades each LED up and down in turn."
  (let ((*piglow-show-automatically* t))
    (loop for led from 1 to 18
          do (progn
               (loop for i from 0 to *demo-max-brightness*
                     do (piglow-set-led led i))
               (loop for i from *demo-max-brightness* downto 0
                     do (piglow-set-led led i))))))

(defun demo-cycle-colours ()
  "Fades colours up and down, starting from red and ending with white."
  (let ((*piglow-show-automatically* t)
        (colours +led-colour-names+))
    (loop for clridx from 0 to (1- (length colours))
          do (loop for i from 0 to *demo-max-brightness*
                   with lastclridx = (1- clridx)
                   do (progn
                        (when (>= lastclridx 0)
                          (piglow-set-colour (aref colours lastclridx) (- *demo-max-brightness* i)))
                        (piglow-set-colour (aref colours clridx) i))))
    (loop for i from *demo-max-brightness* downto 0
          do (piglow-set-colour :white i))))

(defun demo-circular-colours ()
  "Like DEMO-CYCLE-COLOURS, but fades each LED in a coloured ring individually as well."
  (let ((colours +led-colour-names+)
        (*piglow-show-automatically* nil))
    (labels ((colour (clr) (aref colours clr))
             (nth-coloured-led (clr led) (nth led (cdr (assoc (colour clr) +led-colours+)))))
      (loop
        for clridx from 0 to (1- (length colours))
        do (loop
             for n-led from 0 to 2
             do (loop
                  for i from 0 to *demo-max-brightness*
                  with lastclridx = (1- clridx)
                  do (progn
                       (when (>= lastclridx 0)
                         (piglow-set-led (nth-coloured-led lastclridx n-led) (- *demo-max-brightness* i)))
                       (piglow-set-led (nth-coloured-led clridx n-led) i)
                       (piglow-show))))))
      (loop for i from *demo-max-brightness* downto 0
            do (progn
                 (piglow-set-colour :white i)
                 (piglow-show))))))
