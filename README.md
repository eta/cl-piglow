# `cl-piglow`, the PiGlow library for Common Lisp

## What is this?

The [PiGlow][pig] is a nice little £10 PCB for the [Raspberry Pi][rpi] with 18 coloured
LEDs on it, which can be individually controlled via software -- and that's where `cl-piglow`
comes in! This library lets you send commands to your PiGlow from the glorious
[Common Lisp][cl] programming language, using a relatively simple API.

The library works somewhat like the [official Python library][pypig], so, if you've used that
one before, this one should be pretty easy to pick up!

[pig]: https://shop.pimoroni.com/products/piglow
[rpi]: https://www.raspberrypi.org/
[cl]: https://common-lisp.net/

## How to use

### Compatibility note

This implementation only works with [Clozure Common Lisp (CCL)][ccl] and
[Steel Bank Common Lisp (SBCL)][sbcl]. It was developed on CCL (and hasn't actually
been tested with SBCL yet, but it should work!).

We recommend CCL -- also because, if you're going to run this on your Pi, CCL supports
threading, whereas SBCL doesn't yet.

[ccl]: https://ccl.clozure.com/
[sbcl]: http://www.sbcl.org/ 

### Installation instructions

1. Download the library, and put it somewhere ASDF can find it. (For example, `git clone`-ing the repo into `~/common-lisp/` should do the trick.)
2. Install the dependency `osicat`. (If [Quicklisp][ql] is installed, running `(ql:quickload :cl-piglow)` in your Lisp REPL should do this, and load the library as well!)
3. If you didn't do it with Quicklisp above, run `(asdf:load-system :cl-piglow)` and watch the compiler output scroll by.
4. Next, you'll need to get the PiGlow I2C device working correctly. *This varies from system to system -- if you've already figured this out, jump to step X. What follows is what I did for my Raspberry Pi Zero, running Arch Linux ARM.*
5. Ensure the line `dtparam=i2c_arm=on` is present in `/boot/config.txt`, and reboot if necessary.
6. Check in `/dev/` for some device called `/dev/i2c-1` (or `/dev/i2c-0` on older Pis). If you've got something there, great! (If not, I can't help you, sorry! Google is your friend...)
7. Make sure that device file is writable by the user you're currently logged in as. On my system, this entailed putting `SUBSYSTEM=="i2c-dev", MODE="0777"` in `/etc/udev/rules.d/99-i2c.rules`, and rebooting.
8. Run `$ i2cdetect -y -q 1`. You should see `54` somewhere in the output. (If not, something's wrong, or you need to substitute `1` for `0`. Again, Google is your friend...)
9. Go on to the instructions below about using the library!

**Tip:** Running the 'easy installer' for the [official Python library][pypig] might help with
making sure the I2C device is set up correctly, if you're on Raspbian. However, I haven't
tried it myself!

[pypig]: https://github.com/pimoroni/piglow
[ql]: https://www.quicklisp.org/beta/

### Basic usage

Set up the library with `OPEN-PIGLOW` (specifying the number of your I2C device, if it isn't `1`, as the first argument):

```lisp
CL-USER> (piglow:open-piglow)
#<BASIC-BINARY-IO-STREAM ISO-8859-1 (CHARACTER-SPECIAL/9) #x15237676>
CL-USER> (piglow:open-piglow 0) ; for older Raspberry Pi revisions only
```

Then, use the commands `piglow-set-colour COLOUR VALUE`, `piglow-set-leg LEG VALUE`, and `piglow-set-led LED VALUE` to make the lights turn off and on.

Available colours are `:red`, `:orange`, `:yellow`, `:green`, `:blue`, `:white`, and `:all`. LEDs are numbered according to [this chart][ledn].

**NB:** You need to call `piglow-show` after issuing any commands if you want the lights to actually change (which enables you to draw a pattern and then show it without any flickering as the pattern is drawn). If you don't like this behaviour, set the value of `*piglow-show-automatically*` to `T`, and it'll be called automatically.

[ledn]: https://github.com/Boeeerb/PiGlow/blob/master/LED_addressing.jpg

Example session:

```lisp
CL-USER> (piglow:piglow-set-colour :green 10) ; turn the green ring on
                                              ; with brightness 10
NIL
CL-USER> (piglow:piglow-show)                 ; make changes visible
NIL
CL-USER> (setf piglow:*piglow-show-automatically* t) ; automatically make changes visible from now on
T
CL-USER> (piglow:piglow-set-colour :red 10)   ; turn the red ring on
NIL
CL-USER> (piglow:piglow-set-leg 1 10)         ; turn the second (rightmost) leg on
NIL
CL-USER> (piglow:piglow-set-led 7 255)        ; turn LED 7 (the rightmost red LED) on
NIL
CL-USER> (piglow:piglow-set-colour :all 0)    ; turn everything off
NIL
```

### Demo functions

There are currently two pretty demos that make your PiGlow light up in various ways to
demonstrate the library's functionality. These are:

- `(piglow:demo-fade-each-led)`: fades each LED up and down individually
- `(piglow:demo-cycle-colours)`: fades colours in and out, in a kinda cool wave effect

Feel free to try them out! (The maximum brightness is adjustable as the `piglow:*demo-max-brightness*` value, if you want to tone it down a bit.)

### Documentation

Is currently in the form of docstrings. Use the Source, Luke!
(Seriously, I've actually written some somewhat coherent docstrings. Docstrings are good.)

## License

MIT
