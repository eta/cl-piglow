;;; PiGlow library (https://shop.pimoroni.com/products/piglow) for Common Lisp

(in-package :cl-piglow)

(defvar *piglow-show-automatically* nil
  "Controls whether PIGLOW-SHOW is automatically called at the end of any PiGlow operation, immediately making the results of that operation visible.
If T, PIGLOW-SHOW will be called automatically.
If NIL, PIGLOW-SHOW must be called in order for the results of PiGlow operations to become visible.")

(defvar *piglow* nil
  "Global variable to store the PiGlow I2C stream in.")

(defvar +i2c-slave+ #x703
  "ioctl() argument I2C_SLAVE")
(defvar +piglow-address+ #x54
  "I2C address of a typical PiGlow.")

(defvar +led-addresses+
  #(#x07 #x08 #x09 #x06 #x05 #x0A ; First (top) leg, out->in
    #x12 #x11 #x10 #x0E #x0C #x0B ; Second (right) leg, likewise
    #x01 #x02 #x03 #x04 #x0F #x0D ; Third (left) leg, likewise
    )
  "Vector of SN3218 LED addresses for LEDs on the PiGlow.")
(defvar +led-colours+
  '((:red . (1 7 13))
    (:orange . (2 8 14))
    (:yellow . (3 9 15))
    (:green . (4 10 16))
    (:blue . (5 11 17))
    (:white . (6 12 18))
    (:all . (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18)))
  "Association list mapping colour names to indices of the +LED-ADDRESSES+ array.")
(defvar +led-colour-names+
  #(:red :orange :yellow :green :blue :white)
  "Vector of LED colour names, from the outmost to the inmost ring.")

(defvar +enable-output+ #x00)
(defvar +update+ #x16)
(defvar +reset+ #x17)
(defvar +enable-leds-1+ #x13)
(defvar +enable-leds-2+ #x14)
(defvar +enable-leds-3+ #x15)
(defvar +piglow-reset-sequence+
  `(
    ;; Reset all SN3218 registers
    (,+reset+ . #xFF)
    ;; Enable output (i.e. turn the thing on)
    (,+enable-output+ . #x01)
    ;; Turn on all the LEDs; we'll modify the value with the
    ;; PWM registers.
    (,+enable-leds-1+ . #xFF)
    (,+enable-leds-2+ . #xFF)
    (,+enable-leds-3+ . #xFF)
    )
  "Sequence of I2C commands used to reset the PiGlow.")

(defun stream-fd (strm)
  "Get the underlying file descriptor for the stream STRM.
XXX: This only works on SBCL and CCL for now (!)"
  #+sbcl (sb-sys:fd-stream-fd strm)
  #+ccl (ccl:stream-device strm :io)
  #-(or sbcl ccl) (error "Common Lisp implementation not supported"))

(defun i2cize-stream (st i2c-address)
  "Configure the stream ST to support I2C commands addressed to I2C-ADDRESS."
  (osicat-posix:ioctl (stream-fd st) +i2c-slave+ (cffi:make-pointer i2c-address)))

(defun open-i2c-device (devid i2c-address)
  "Open the I2C device /dev/i2c-DEVID, returning a stream supporting I2C commands addressed to I2C-ADDRESS."
  (let ((ret (open (format nil "/dev/i2c-~A" devid)
                   :direction :io
                   :if-exists :overwrite
                   #+ccl :sharing #+ccl :lock ; CCL is edgy and requires this for multithreaded access
                   :element-type '(unsigned-byte 8))))
    (i2cize-stream ret i2c-address)
    ret))

(let ((buf (make-array 2 :element-type '(unsigned-byte 8))))
  (defun write-i2c-octet (device register value)
    "Write the value VALUE to the I2C register REGISTER, using DEVICE."
    (declare (type (unsigned-byte 8) register value) (type stream device)
             (optimize (speed 3)))
    (setf (aref buf 0) register)
    (setf (aref buf 1) value)
    (write-sequence buf device)
    (force-output device)))

(defun write-i2c-octets (device alist)
  "Write each (register, value) pair in ALIST to I2C device DEVICE."
  (loop for cell in alist
        do (write-i2c-octet device (car cell) (cdr cell))))

(defun piglow-reset (&optional (stream *piglow*))
  "Reset all the LEDs on the PiGlow by reinitalizing the device.
WARNING: Has immediate effect, regardless of the value of `*PIGLOW-SHOW-AUTOMATICALLY*'.
If you just want to turn everything off, try `(piglow-set-colour STREAM :all 0)'"
  (write-i2c-octets stream +piglow-reset-sequence+))

(defun open-piglow (&optional (devid 1) (address +piglow-address+) (update-global-variable t))
  "Open a connection to the PiGlow represented by I2C device /dev/i2c-DEVID, returning a stream used to write commands to it.
If UPDATE-GLOBAL-VARIABLE is T, updates the value of *PIGLOW* with the new stream."
  (let ((ret (open-i2c-device devid address)))
    (piglow-reset ret)
    (when update-global-variable (setf *piglow* ret))
    ret))

(defun close-piglow (&optional (stream *piglow*))
  "Close the connection to the PiGlow specified by STREAM."
  (close stream))

(defun piglow-show (&optional (stream *piglow*))
  "Make the PiGlow do what you just told it to; i.e. execute any pending commands.
See also: the *PIGLOW-SHOW-AUTOMATICALLY* variable, which controls whether this function gets called for you."
  (write-i2c-octet stream +update+ #xff))

(defun piglow-maybe-show-automatically (&optional (stream *piglow*))
  (when *piglow-show-automatically* (piglow-show stream)))

(defun piglow-set-led (led value &optional (stream *piglow*))
  "Sets LED (an index into +LED-ADDRESSES+) to VALUE (0-255) on the PiGlow.
Signals an error if the LED supplied doesn't exist."
  (declare (type (unsigned-byte 8) led value)
           (optimize (speed 3)))
  (let ((address (aref +led-addresses+ (1- led))))
    (assert address () "Provided LED number ~A doesn't exist" led)
    (write-i2c-octet stream address value)
    (piglow-maybe-show-automatically stream)))

(defun piglow-set-leg (leg value &optional (stream *piglow*))
  "Sets all LEDS on leg LEG (an integer from 0 to 2) to VALUE (0-255) on the PiGlow.
Signals an error if the value of LEG is invalid."
  (declare (type (unsigned-byte 8) leg value)
           (optimize (speed 3)))
  (assert (and (< leg 3) (>= leg 0)) (leg) "Leg value ~A must be between 0 and 2" leg)
  (let ((*piglow-show-automatically* nil)) ; We want to do the show at the end of this function!
    (loop for led from (1+ (* leg 6)) to (+ 6 (* leg 6))
          do (piglow-set-led led value stream)))
  (piglow-maybe-show-automatically stream))


(defun piglow-set-colour (colour value &optional (stream *piglow*))
  "Sets all LEDS of colour COLOUR (a key of the alist +LED-COLOURS+) to VALUE (0-255) on the PiGlow.
Signals an error if the colour supplied doesn't exist."
  (declare (type (unsigned-byte 8) value)
           (symbol colour)
           (optimize (speed 3)))
  (let ((colours (assoc colour +led-colours+)))
    (assert colours () "Provided colour ~A doesn't exist" colour)
    (let ((*piglow-show-automatically* nil)) ; We want to do the show at the end of this function!
      (let ((each (lambda (x) (piglow-set-led x value stream))))
        (declare (dynamic-extent each))
        (mapc each (cdr colours))))
    (piglow-maybe-show-automatically stream)))
