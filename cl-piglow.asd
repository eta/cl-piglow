(defsystem "cl-piglow"
  :name "cl-piglow"
  :description "A Pimoroni PiGlow library for Common Lisp"
  :version "0.0.1"
  :author "eta <https://theta.eu.org>"
  :license "MIT"
  :depends-on ("osicat")
  :serial t
  :components
  ((:file "packages")
   (:file "piglow")
   (:file "demos")))

